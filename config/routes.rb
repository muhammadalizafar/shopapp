Rails.application.routes.draw do
  resources :members
  devise_for :users
  resources :products
  resources :orders

  root to: "products#index"
  
  post 'products/add_to_cart/:id', to: 'products#add_to_cart', as: 'add_to_cart'
  # post 'add_to_cart' => "products#add_to_cart"

  post 'products/remove_item/:id', to: 'products#remove_item', as: 'remove_item'
  # post 'products/add_to_cart/:id', to: 'products#add_to_cart'

  # post 'orders/new_order/:id', to: 'orders#new', as: 'new_order'

  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
  post 'checkout/create', to: 'checkout#create'

  post 'payments/checkout'

end
