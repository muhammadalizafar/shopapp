class ProductsController < ApplicationController
    before_action :intilize_session
    before_action :load_cart
    
    def index
      @products = Product.all
    end
  
    def new
      @product = Product.new
    end
  
    def create
      @product = Product.create(product_params)
  
      respond_to do |format|
        if @product.save
          format.html { redirect_to products_path, notice: 'Product was successfully created.' }
          format.js
          format.json { render json: @product, status: :created, location: @product }
        else
          format.html { render action: "new" }
          format.json { render json: @product.errors, status: :unprocessable_entity }
        end
      end
    end
  
    def edit
      @product = Product.find(params[:id])
    end
  
    def update
      @product = Product.find(params[:id])
      @product.update(product_params)
      redirect_to products_path
    end
  
    def destroy
      @product = Product.find(params[:id])
      @product.destroy
      {notice: "Product deleted successfully" }
  
      respond_to do |format|
        format.html { redirect_to products_path}
        format.json { head :no_content }
        format.js   { render :layout => false }
     end
    end
  
    def add_to_cart
      param = params[:id]
      session[:cart] << param
      # if session[:cart].include?(param)
      #   flash.alert = "Already added."
      # end
      redirect_to products_path  
    end
  
    def remove_item
      param = params[:id]
      session[:cart].delete(param)
      redirect_to products_path
    end
    
    private
      def product_params
        params.require(:product).permit(:name, :price, :image)
      end
  
      def intilize_session
        # if session[:cart]
        #     @cart = session[:cart].find_by(:id => session[:cart])
          # if @cart.present?
        session[:cart] ||= []
      end
  
      def load_cart
        @cart = Product.find(session[:cart])
      end
  
        # end
      # end
  end