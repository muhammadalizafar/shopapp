class OrdersController < ApplicationController
  def index
    # u = User.find(:id)
    @orders = Order.all
    # @user = User.find(user_id)
    # @orders = Order.find(params[:id])
    # @orders = Order.current_scope
    # products = Product.all
  end

  def show
    # @orders = Order.find(params[:id])
    # u = User.find_by(:id)
    # respond_with User.find(params[:id])

    @order = current_user.orders
  end

  def new
    @orders = Order.new
  end

  def create
    # @ordars = Ordar.new
      # byebug
    @orders = Order.create(order_params)
    # @cart.quantity = params[:quantity]
    # @cart.products << Product.find(params[:product_id])

    # #@cart = Cart.create(cart_params)
    # @cart.save

    @cart = Product.find(session[:cart])
    # binding.pry
    @orders.products << @cart
    @orders.save

    session[:cart] = []
    
    redirect_to products_path
    # @ordar = Ordar.new(order_params)
    # @current_cart.line_items.each do |item|
    #   @order.line_items << item
    #   item.cart_id = nil
    # end
    # @order.save
    # Cart.destroy(session[:cart_id])
    # session[:cart_id] = nil
    # redirect_to root_path
  end

  def destroy
    @orders = Order.find(params[:id])
    @orders.destroy
    redirect_to orders_path
  end

  private
  # binding.pry
    def order_params
      params.require(:order).permit(:name, :email, :address, :user_id)
    end

    # def order_by_id
    #   u = User.find_by(id:3)
    #   u.orders
    # end
end
