class PaymentsController < ApplicationController
  
  def index
  end

  def new
    @client_token = gateway.client_token.generate
  end

  def checkout
    nonce_from_the_client = params[:payment_method_nonce]

    result = gateway.transaction.sale(
      :amount => price,
      :payment_method_nonce => nonce_from_the_client,
      :device_data => device_data_from_the_client,
      :options => {
        :submit_for_settlement => true
      }
    )

    if result.success?
      puts "success!: #{result.transaction.id}"
    elsif result.transaction
      puts "Error processing transaction:"
      puts "  code: #{result.transaction.processor_response_code}"
      puts "  text: #{result.transaction.processor_response_text}"
    else
      p result.errors
    end

    begin
      customer = gateway.customer.create!(:first_name => "Josh")
      puts "Created customer #{customer.id}"
    rescue Braintree::ValidationsFailed
      puts "Validations failed"
    end
    
  end


  def gateway
    env = ENV["BT_ENVIRONMENT"]

    @gateway ||= Braintree::Gateway.new(
      :environment => env && env.to_sym,
      :merchant_id => ENV["BT_MERCHANT_ID"],
      :public_key => ENV["BT_PUBLIC_KEY"],
      :private_key => ENV["BT_PRIVATE_KEY"],
    )
  end
end
