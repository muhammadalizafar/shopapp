class MembersController < Devise::RegistrationsController
  def index
    @users = User.all
end

private
def user_params
    params.require(resource).permit(:email, :password, :password_confirmation, :full_name)
end 
end
