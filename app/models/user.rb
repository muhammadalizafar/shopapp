class User < ApplicationRecord
  rolify strict: true
  has_many :orders
  has_many :products
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable
	
         after_create :assign_default_role
  
  def assign_default_role
    self.add_role(:newuser) if self.roles.blank?
  end

  after_create do
    result = Braintree::Customer.create(
      # :first_name => "Jen",
      # :last_name => "Smith",
      # :company => "Braintree",
      :email => email,
    )
    update(braintree_customer_id: result.customer.id)
  end

end
