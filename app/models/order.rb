class Order < ApplicationRecord
    has_many :products
    has_one :user
end
